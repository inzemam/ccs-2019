\section{Intel x86/64 with VMX Support}
\label{sec:processor}

In this section we give a high-level view of the x86/64
processor platform, on which the Muen SK runs.
We abstract some components of the system to simplify our
model of the processor system that Muen generates.
For a more complete description of the platform we refer 
% the reader 
to the % voluminous but 
excellent reference \cite{intel2018}.

The lower part of Fig.~\ref{fig:muen-system} depicts the 
processor system and its components.
The CPU components (with the 64-bit general purpose registers,
including the instruction pointer, stack pointer, and control
registers, as well as model-specific registers like the Time Stamp
Counter (TSC)) and physical memory components are standard.
The layer above the CPUs shows components like the VMCS pointer
(VMPTR), the VMX-Timer, and extended page table pointer (EPTP),
which are part of the VT-x layer of the
Virtual Machine Extension (VMX) mode, that supports virtualization.
The VMPTR component on each CPU points to a VMCS structure, which
is used by the Virtual Machine Monitor (VMM) (here the kernel) to
control the launching and exiting of guest processes or ``subjects.''
The CR3 register (which is technically one of the control registers in the CPU
component) and the EPTP component (set by the active VMCS pointed
to by the VMPTR) control the
virtual to physical address translation for instructions that
access memory.
On the top-most layer we show the kernel code (abstracted as a
program) that runs on each CPU. The kernel code has two
components: an Init component that runs on system initialization,
and a Handler component that handles VM exits due to interrupts.

We are interested in the VMX mode operation of this processor,
in which the kernel essentially runs as a VMM, and subjects run as
guest software on Virtual Machines 
(VMs) provided by the VMM. Subjects could
be bare-metal application programs, or a guest operating system
(called a VM-subject in Muen).
%% In VMX mode, the processor can be in one of two modes: \emph{root}
%% mode, in which the kernel typically runs, or \emph{non-root} mode
%% in which the guest software runs.
%% The processor goes from root mode to non-root mode when the kernel
%% ``launches'' a guest software in a VM.
%
A VM is specified using a VM Control Structure (VMCS), which
stores information about the guest processor state including the
IP, SP, and the CR3 control register values. It also stores values
that control the execution of a subject, like the VMX-timer which sets the
time slice for the subject to run before the timer causes it to
exit the VM, and the
extended page table pointer (EPTP) which translates guest physical
addresses to actual physical addresses. It also contains the
processor state of the host (the kernel).
To launch a subject in a VM, the kernel sets the VMPTR to point to
one of the VMCSs (from an array of VMCSs shown in the figure) using
the VMPTRLD instruction, and then calls VMLAUNCH.
The launch instruction can be thought of as setting the Timer, CR3, and
EPTP components in the VT-x layer, from the VMCS fields.
A subject is caused to exit its VM and return control to the
kernel (called a VM exit), by certain events like VMX-timer
expiry, page table exceptions, and interrupts.
%% A VMLAUNCH causes a transition from root mode to non-root mode, while
%% an VM-Exit has the opposite effect.

We would like to view such a processor system as a machine of the type
described in Sec.~\ref{sec:parametric-refinement}.
The state of the machine is the contents of all its components.
The main operations here are as follows.
\begin{enumerate}
\item \emph{Init}: The init code of the kernel is executed on each of
  the processors, starting with CPU0 which we consider to be the
  bootstrap processor (BSP).
\item \emph{Execute}: This operation takes a CPU id and executes
  the next instruction pointed to by the IP on that CPU. The
  instruction could be one that does not access memory, like an
  instruction that adds the contents of one register into
  another, which only reads and updates the register state on that CPU.
  Or it could be an instruction that accesses
  memory, like moving a value in
  a register $R$ to a memory address $a$. The address $a$ will be
  translated via the page tables pointed to by the CR3 and EPTP components,
  successively, to obtain an address in physical memory which will
  then be updated with the contents of register $R$. Some instructions
  may cause an exception (like an illegal memory access), in which
  case we assume the exception handler runs as part of this
  operation.
  \item \emph{Event}: We consider three kinds of events (or
    interrupts). One is the timer tick event on a CPU. This causes the
    Time-Stamp Counter (TSC) on the CPU to increment, and also
    decrements the VMX-Timer associated with the active VM. If the
    VMX-Timer becomes 0, it causes a VM exit, which is then processed
    by the corresponding handler.
    Other events include those generated by a VMCALL instruction, and
    external interrupts. Both these cause a VM exit. The cause of all
    VM exits is stored in the
    subject's VMCS, which the handler checks and takes appropriate
    action for.
\end{enumerate}


\begin{figure*}
\centering
% \vspace{-0.5cm}
\input{muen-system-v3.pdf_t}
%\scalebox{0.66}{\input{virtualized-system.pdf_t}}
\caption{Components of an x86/64bit Processor System with VMX
  Support. Shaded portions are generated by Muen.}
\label{fig:muen-system}
\end{figure*}


%% Let us take timer expiry event as examples to understand this.

%% Timer expiry is a privileged event. There is a timer in VT-x, called VMX preemption timer. Whenever this timer value becomes 0, a VM exit happens. This, we call timer expiry. The VMM or hypervisor looks at the reason of exit in the VMCS. And then the handler part of the VMM or hyervisor schedules the next guest OS to run. And then gives control back to the guest OS.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main.tex"
%%% End:
