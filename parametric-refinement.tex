\section{Conditional Parametric Refinement} 
\label{sec:parametric-refinement}

We begin by introducing the flavour of classical refinement that
we will make use of, followed by the parametric refinement
framework % which is the main technique 
we employ for our verification task.

\subsection{Machines and Refinement}
% \paragraph{Machines and Refinement.}
A convenient way to reason about systems such as Muen is to view
them as an \emph{Abstract Data Type} or simply \emph{machine} to
use the Event-B terminology \cite{abrial2010}.
A \emph{machine type} is a finite set of named operations
$\N$, with each operation $n \in \N$ having an
associated input domain $I_n$ and output domain $O_n$.
Each machine type contains a designated initialization
operation called \init.
A \emph{machine} $\A$ of type $\N$ is a structure of the
form $(Q,\{\op_n\}_{n\in \N})$, where $Q$ is a set of states, and for
each $n \in \N$,
$\op_n : (Q \times I_n) \rightarrow (Q \times O_n)$
is the implementation of operation $n$.
If $\op_n(p,a) = (q,b)$, then when $\op_n$ is invoked with input $a$ in
a state $p$ of the machine, it returns $b$ and changes the state
to $q$.
%% The \init\ operation is assumed to be independent of the state in which it is
%% invoked as well as the input passed to it.
%% Hence we simply write $\init()$ instead of $\init(p,a)$ etc.

The machine $\A$ induces a transition system $\T_{\A}$ in a
natural way, whose states are the states $Q$ of $\A$, and
transitions from one state to another are labelled by triples of
the form $(n,a,b)$, representing that operation $n$ with input $a$ was
invoked and the return value was $b$.
One is interested in the language of \emph{initialized}
sequences of operation calls produced by this transition system, which
models behaviours of the system,
and we call it $\linit(\A)$.

We will consider different ways of representing machines, the most
prominent of which is as a program in a high-level imperative
programming language. % (or even assembly). 
The global variables of the
program (more precisely \emph{valuations} for them) make up the state
of the machine.
The implementation of an operation $n$ is given by a method definition
of the same name, that takes an argument $a$ in $I_n$, updates the
global state, and returns a value $b$ in $O_n$.
We call such a program a \emph{machine program}.
%
Fig~\ref{fig:program-parametric}(a) shows a program in a C-like
language, that represents a
``set'' machine with operations $\init$, $\mathit{add}$
and $\mathit{elem}$.
The set stores a subset of the numbers 0--3, in a Boolean array of size
4. However, for certain extraneous reasons, it uses an array $T$ to permute the
positions where information for an element $x$ is stored.
Thus to indicate that $x$ is present in the
set the bit $S[T[x]]$ is set to true.

Another representation of a machine could be in the form of a
processor-based system. Here the state is given by the values of
the processor's registers and the contents of the memory. The
operations (like ``execute the next instruction on CPU0'', or ``timer
event on CPU1'') are defined by either the processor
hardware (as in the former operation) or by software in the form of an
interrupt handler (as in the latter operation).

\begin{figure*}[th]
\hspace{-1.2cm}
\begin{minipage}[t]{4.1cm}
\begin{small}
\begin{verbatim}
bool S[4];
unsigned T[4] := {1,2,3,0};

void init() {
  for (int i:=0; i<4; i++)
    S[i] := false;
}
   
void add(unsigned x) {
  if (x < 4)
    S[T[x]] := true;
}

bool elem(unsigned x) {
  return (x < 4 && S[T[x]]);
}
\end{verbatim}
\end{small}
\end{minipage}
% Abstract Spec
\begin{minipage}[t]{3.6cm}
\begin{small}
\begin{verbatim}
// Abstract spec
bool absS[4];
void add(unsigned x) {
  if (x < 4)
    absS[x] := true;
}

...

// Gluing relation
\forall unsigned x:
  (x < 4 => 
     S[T[x]] = absS[x])
\end{verbatim}
\end{small}
\end{minipage} 
% Parametric Prog Q[Usize,T]
\begin{minipage}[t]{4.7cm}
\begin{small}
\begin{verbatim}
bool S[UINTMAX];
unsigned T[UINTMAX];
unsigned Usize;

void init() {
  for (int i:=0; i<Usize; i++)
    S[i] := false;
}
   
void add(unsigned x) {
  if (x < Usize)
    S[T[x]] := true;
}

bool elem(unsigned x) {
  return (x < Usize 
           && S[T[x]]);
}
\end{verbatim}
\end{small}
\end{minipage}
% Abst Param Spec
\begin{minipage}[t]{4cm}
\begin{small}
\begin{verbatim}
// Abstract parametric spec
bool absS[UINTMAX];
unsigned absUsize;

void add(unsigned x) {
  if (x < absUsize)
    absS[x] := true;
}

...

// Assumption R: Usize = absUsize 
     && T injective

// Parametric gluing predicate
\forall unsigned x: 
  (x < Usize => S[T[x]] = absS[x])
\end{verbatim}
\end{small}
\end{minipage} \\[0.3cm]
\hspace{-1.4cm}
\begin{minipage}[b]{4.1cm}
\begin{center}
\begin{small}
(a)
\end{small}
\end{center}
\end{minipage}
%
\begin{minipage}[b]{3.6cm}
\begin{center}
\begin{small}
(b)
\end{small}
\end{center}
\end{minipage}
%
\begin{minipage}[b]{4.7cm}
\begin{center}
\begin{small}
(c)
\end{small}
\end{center}
\end{minipage}
%
\begin{minipage}[b]{4cm}
\begin{center}
\begin{small}
(d)
\end{small}
\end{center}
\end{minipage}
\caption{(a) A program $P$ implementing a set machine, 
  (b) an abstract specification $A$ and gluing relation, 
  (c) a parametric program $Q[\size,T]$ representing a parametric
      set machine, and 
  (d) the abstract parametric specification and gluing predicate.}
\label{fig:program-parametric}
\end{figure*}

%% \begin{figure*}[th]
%% \begin{minipage}[t]{3.6cm}
%% \begin{scriptsize}
%% \begin{verbatim}
%% bool S[4];
%% unsigned T[4] := {1,2,3,0};

%% void init() {
%%   for (int i:=0; i<4; i++)
%%     S[i] := false;
%% }
   
%% void add(unsigned x) {
%%   if (x < 4)
%%     S[T[x]] := true;
%% }

%% bool elem(unsigned x) {
%%   return (x < 4 
%%             && S[T[x]]);
%% }
%% \end{verbatim}
%% \end{scriptsize}
%% \end{minipage}
%% %
%% \begin{minipage}[t]{3.8cm}
%% \begin{scriptsize}
%% \begin{verbatim}
%% bool S[UINTMAX];
%% unsigned T[UINTMAX];
%% unsigned Usize;

%% void init() {
%%   for (int i:=0; i<Usize;i++)
%%     S[i] := false;
%% }
   
%% void add(unsigned x) {
%%   if (x < Usize)
%%     S[T[x]] := true;
%% }

%% bool elem(unsigned x) {
%%   return (x < Usize 
%%             && S[T[x]]);
%% }
%% \end{verbatim}
%% \end{scriptsize}
%% \end{minipage}
%% \begin{minipage}[t]{4cm}
%% \begin{scriptsize}
%% \begin{verbatim}
%% // Abstract spec
%% bool absS[4];
%% void add(unsigned x) {
%%   if (x < 4)
%%     absS[x] := true;
%% }
%% ...
%% // Gluing relation
%% \forall unsigned x:
%%   (x < 4 => S[T[x]] = absS[x])
%% --------
%% // Abstract parametric spec
%% bool absS[UINTMAX];
%% unsigned absUsize;
%% ...
%% // Assumption: Usize = absUsize && T injective
%% // Parametric gluing predicate
%% \forall unsigned x: 
%%   (x < Usize => S[T[x]] = absS[x])
%% \end{verbatim}
%% \end{scriptsize}
%% \end{minipage} \\[0.3cm]
%% \begin{minipage}[b]{3.6cm}
%% \begin{center}
%% \begin{scriptsize}
%%   (a)
%% \end{scriptsize}
%% \end{center}
%% \end{minipage}
%% %
%% \begin{minipage}[b]{3.8cm}
%% \begin{center}
%% \begin{scriptsize}
%%   (b)
%% \end{scriptsize}
%% \end{center}
%% \end{minipage}
%% %
%% \begin{minipage}[b]{4cm}
%% \begin{center}
%% \begin{scriptsize}
%%   (c)
%% \end{scriptsize}
%% \end{center}
%% \end{minipage}
%% \caption{(a) A program $P$ implementing a set machine, (b) a
%%   parametric program $Q[\size,T]$ representing a parametric
%%   set machine, and (c) the abstract specifications.}
%% \label{fig:program-parametric}
%% \end{figure*}

Refinement is a way of saying that a ``concrete'' machine
conforms to an ``abstract'' one, behaviourally. 
We use a strong notion of refinement which---in our setting
of total deterministic machines---leads to equivalence of sequential
behaviours.
%
Let $\A = (Q, \{\op_n\}_{n\in \N})$  and $\A' = (Q', \{\op_n'\}_{n\in
  \N})$ be two machines of type $\N$.
We say $\A'$ \emph{refines} $\A$ if there exists a relation $\rho
\subseteq Q' \times Q$, called a \emph{gluing relation}, satisfying
the following conditions:
\begin{itemize}
\item(init) Let $\op_{\init}() = q_0$ and $\op'_{\init}() =
  q_0'$.
  Then we require that $(q_0',q_0) \in \rho$.
  Thus, after the
  machines are initialized, their states must be $\rho$-related.
\item(sim) Let $p \in Q$, and $p' \in Q'$, with $(p',p) \in
  \rho$. Let $n \in \N$, $a \in I_n$, and suppose
  $\op_n(p,a) = (q, b)$ and $\op'_n(p',a) = (q', b')$. Then we must
  have $b=b'$ and $(q',q) \in \rho$.
\end{itemize}

If $\A'$ refines $\A$, then every initialized sequence of operation
calls in $\A'$ can be mimicked by $\A$; and vice-versa. It follows
that $\linit(\A') = \linit(\A)$.

When machines are presented in the form of programs, we can use
Floyd-Hoare logic based % code-level 
verification tools (like VCC \cite{VCC} for C, or GNAT Pro
\cite{gnatpro} for Ada Spark), to phrase refinement conditions
as pre/post annotations and carry out a
machine-checked proof of refinement \cite{DivakaranDS14}.
The basic idea is to combine both the abstract and concrete programs
into a single ``combined'' program with separate state variables but
joint methods 
that carry out the abstract and concrete operation one after the
other.
The gluing relation is specified as a predicate on the combined state.
Fig.~\ref{fig:program-parametric}(b) shows an abstract specification
and a gluing relation, for the set machine program of part~(a).
The refinement conditions (init) and (sim) are phrased as pre/post
annotations on the joint operation methods.

\subsection{Generative Systems and Parametric Refinement}
% \paragraph{Generative Systems.}
A \emph{generative system} is a program $G$ that given an input
specification $I$ (in some space of valid inputs), generates a machine
program $P_I$.
As an example, one can think of a set machine generator $\setgen$, that given a
number $n$ of type unsigned int (representing the universe size),
generates a program $P_n$ similar to
the one in Fig.~\ref{fig:program-parametric}(a), which uses the
constant $n$ in place of the set size 4, and an array $T_n$ of size
$n$, which maps each $x$ in $[0..n-1]$ to $(x+1)\!\!\!\mod n$.

For every $I$, let us say we have an abstract machine (again similar
to the one in Fig.~\ref{fig:program-parametric}(b)) say
$A_I$, describing the intended behaviour of the machine $P_I$.
Then the verification problem of interest to us, for the generative
system $G$, is to show that for \emph{each} input
specification $I$, $P_I$ refines $A_I$.
This is illustrated in Fig.~\ref{fig:generative-proof}(a).
%
We propose a way to address this problem using refinement of
\emph{parametric} programs, which we describe next.

%\subsection{Parametric Refinement}
\paragraph{Parametric Refinement.}

A \emph{parametric} program is like a standard program, except that
it has certain read-only variables which are left
\emph{uninitialized}. These uninitialized variables act like
``parameters'' to the program.
We denote by $P[V]$ a parametric program $P$ with an uninitialized
variable $V$.
As such a parametric program has no useful
meaning (since the uninitialized variables may contain
arbitrary values).
But if we initialize the variable $V$ with a value $v$ passed to the
program, we get a standard program which we denote by $P[v]$.
Thus $P[v]$ is obtained from $P[V]$ by adding the initialization $V :=
v$ soon after the declaration of $V$.
By convention we use uppercase names for parameter variables, and
lowercase names for values passed to the program.
Instead of a single parameter we allow a parametric program to have a
list of parameters, and extend our notation in the expected
way for such programs.
%% A parametric machine program of type $\N$ is simply a parametric
%% program that implements a machine of type $\N$.

Fig.~\ref{fig:program-parametric}(c) shows an example parametric
machine program $Q[\size,T]$, representing a parametric version of the set
program in part~(a).
Given a value 4 for $\size$ and a list $[1,2,3,0]$ for $T$, we get the
program $Q[4,[1,2,3,0]]$, which behaves similar to the program in
part~(a). % of the figure.

Let $P[U]$ and $Q[V]$ be two parametric machine programs of the same
type.
A \emph{parametric gluing predicate} for $P[U]$ and $Q[V]$ is a
predicate $\pi$ on the program variables of $P[U]$ and $Q[V]$. Such
a predicate $\pi$ represents a family of gluing relations, one for
each instantiation of the parameters $U$ and $V$.

Let $R$ be a predicate on the parameter variables $U$ and $V$.
We say $Q[V]$ \emph{refines} $P[U]$ w.r.t.\@ the condition $R$, if
there exists a parametric gluing predicate $\pi$ for $P[U]$ and
$Q[V]$, such that for every instance $u$ of $U$ and $v$ of $V$ which
satisfy $R$ (that is $R(u,v)$ holds),
we have that $Q[v]$ refines $P[u]$ via the gluing relation
given by $\pi$.
The standard way of phrasing the refinement conditions for machine
programs in the form of program annotations, extends easily
to phrasing such a gluing predicate and the refinement conditions for
parametric programs.

Consider the parametric machine program
$Q[\size,T]$ in Fig.~\ref{fig:program-parametric}(c), and the
abstract parametric program in % the bottom part of
Fig.~\ref{fig:program-parametric}(d),
which we call $B[\abssize]$.
Consider the condition $R$ which requires that
$\abssize = \size$ and $T$ to be injective.
Let $\pi$ be the parametric gluing predicate
$\forall x: \mathrm{unsigned}, (x < \size) \implies S[T[x]] = absS[x])$.
Then $Q[\size,T]$ can be seen to refine $B[\abssize]$ with this
predicate $\pi$ w.r.t.\@ the condition $R$.


% \subsection{Verifying Generative Systems using Parametric Refinement}
\paragraph{Verifying Generative Systems using Parametric Refinement.}
Returning to our problem of verifying a generative system, we show a
way to solve this problem using the above framework of conditional
parametric refinement.
Consider a generative system $G$ that given an input specification
$I$, generates a machine program $P_I$, and let
$A_I$ be the abstract specification for input $I$. 
Recall that our aim is to show that for each $I$, $P_I$
refines $A_I$.
Suppose we can associate a parametric program $Q[V]$ with $G$, such
that for each $I$, $G$ can be viewed as generating the value $v_I$ for
the parameter $V$, so that $Q[v_I]$ is behaviourally equivalent to $P_I$.
And similarly a parametric abstract specification $B[U]$, and a
concrete value $u_I$ for each $I$, such that
$A_I$ is equivalent to $B[u_I]$.
Further, suppose we can show that $Q[V]$ parametrically refines $B[U]$
with respect to a condition $R$ on $U$ and $V$.
Then, for each $I$ such that $v_I$ and $u_I$ \emph{satisfy} the condition $R$,
we can conclude that $P_I$ refines $A_I$.
This is illustrated in Fig.~\ref{fig:generative-proof}.
If the condition $R$ is natural enough that a correctly functioning
generator $G$ would satisfy, then this argument would cover all inputs
$I$.
%% Of course, for this technique to be effective our condition $R$ must
%% be permissive enough to cover as many input
%% specifications as possible.

As a final illustration in our running example, to verify the
correctness of the set machine generator $\setgen$, we use the
parametric programs $Q[\size,T]$ and $B[\abssize]$ to capture the
concrete program generated and the abstract specification
respectively.
We then show that $Q[\size,T]$ refines $B[\abssize]$ w.r.t.\@ the
condition $R$, using the gluing predicate $\pi$, as described above.
We note that the actual values generated for the parameters in this
case (recall that these are values for the  parameters $\size$,
$\abssize$ and $T$) do indeed satisfy the conditions required by $R$,
namely that $\size$ and $\abssize$ be equal, and $T$ be injective.
Thus we can conclude that for each input universe size $n$, the
machine program $P_n$ refines $A_n$, and we are done.


\begin{figure}
\centering
\input{generative-proof-v2.pdf_t}
\caption{Proving correctness of a generative system using parametric
  refinement. Part (a) shows the goal, (b) the proof artifacts and
  obligation, and (c) the guarantee. Dashed arrows represent refinement.}
\label{fig:generative-proof}
\end{figure}



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main.tex"
%%% End:



