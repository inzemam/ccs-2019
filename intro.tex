\section{Introduction}
\label{sec:intro}

A separation kernel (SK) is a small specialized operating system or
microkernel, that provides a sand-boxed or ``separate''  execution
environment for a given set of processes (also called ``partitions''
or ``subjects'').
The subjects may communicate only via declared memory channels,
and are otherwise isolated from each other.
Unlike a general operating system % OS
these kernels usually have a
fixed set of subjects to run according to a specific schedule on
the different CPUs of a processor-based system.

The idea of a separation kernel was proposed by Rushby
\cite{rushby} as a way of breaking down the process of reasoning
about the overall security of a computer system.
The overall security of a system, in his view, derives from (a)
the physical separation of its components, and (b) the specific security
properties of its individual components or subjects.
A separation kernel thus focuses on providing the separation property
(a) above.
Separation kernels have since been embraced extensively in military
and aerospace domains, for building security and safety-critical
applications.
%% % Needless to say
%% The correct functioning of the kernel itself is
%% of critical importance in guaranteeing the secure and timely
%% execution of the subjects.

Our focus in this paper is in formally verifying that a separation
kernel does indeed provide the separation property, and more generally that
it functions \emph{correctly} (which would include for example, that
it executes subjects according to a specified schedule).
One way of obtaining a high level of assurance in the
correct functioning of a system is to carry out a refinement-based
proof of functional correctness \cite{Hoare75a,Hoare-tr-1985},
as has been done in the
context of OS verification \cite{rushby,klein}.
Here one specifies an abstract model of
the system's behaviour, and then shows that the system
implementation conforms to the abstract specification.
A refinement proof typically subsumes all the standard security
properties related to separation, like no-exfiltration/infiltration
and temporal and spatial separation of subjects considered for
instance in \cite{heitmeyer}.

Our specific aim in this paper is to formally verify the correctness of the
Muen separation kernel \cite{muen},
which is an open-source representative of a class of modern
separation kernels (including several commercial products like
GreenHills Integrity Multivisor \cite{GHIM},  LynxSecure \cite{Lynx}, PikeOS
\cite{pikeos}, VxWorks MILS platform \cite{Wind}, and XtratuM \cite{xtratum})
that use hardware virtualization support and
are \emph{generative} in nature.
By the latter we mean that these tools take an input specification
describing the subjects and the schedule of execution, and
generate a tailor-made processor-based system that includes subject
binaries, page tables, and a kernel that acts like a Virtual
Machine Monitor (VMM).

When we took up this verification task over three years ago, a
few challenges stood out.
How does one reason about a system whose kernel makes use of
virtualization features in the underlying hardware in addition to
Assembly and a high-level language like Ada?
Secondly, how does one reason about a complex 4-level paging structure and
the translation function it induces?
Finally, and most importantly, how do we reason about a generative
system to show that for \emph{every} possible input specification,
it produces a correct artifact?
A possible approach for the latter would be to verify the generator code, along
the lines of the CompCert project \cite{xavier}.
However with the generator code running close to 80K LOC, with little
or no compositional structure, and not being designed for verification,
this would be a formidable task.
One could alternatively perform translation validation
\cite{pnueli} of an input specification of interest, but this
would require manual effort from scratch each time.

We overcame the first challenge of virtualization by simply
choosing to model the virtualization layer (in this case Intel's
VT-x layer) along with the rest of the hardware components like
registers and memory, programmatically in software.
Thus we modeled VT-x components like the per-CPU VMX-Timer and
EPTP as 64-bit variables in Ada, and implicit structures like the
VMCS as a record with appropriate fields as specified by Intel
\cite{intel2018}.
Instructions like VMLAUNCH were then implemented as methods that
accessed these variables.
As it turned out, virtualization was more of a boon than a bane,
as it simplifies the kernel's (and hence the prover's) job of
preemption and context-switching.

We solved the third problem of generativeness (and coincidentally
the second problem of page tables too), by leveraging a key
feature of such systems: the kernel is essentially a
\emph{template} which is largely fixed, independent of the input
specification.
The kernel accesses variables which represent input-specific details
like subject details and the schedule, and these structures are
generated by Muen based on the given input specification.
The kernel can thus be viewed as a \emph{parametric} program, much like a
method that computes using its formal parameter variables.
In fact, taking a step back, the whole processor system
generated by Muen can be viewed as a parametric program with
parameter values like the schedule, subject details, page tables, and memory
elements being filled in by the generator based on the input
specification.

This view suggests a novel two-step technique for verifying generative
systems that can be represented as parametric programs.
We call this approach \emph{conditional parametric refinement}.
We first perform a general verification 
step (independent of the input spec) to verify that the parametric
program refines a parametric abstract specification,
\emph{assuming} certain natural conditions on the parameter values (for
example \emph{injectivity} of the page tables) that
are to be filled in.
This first step essentially tells us that for \emph{any} input
specification $P$, if the parameters generated by the system generator
% (like the Muen generator) 
satisfy the
assumed conditions, then the generated system is correct vis-a-vis the
abstract specification.
In the second step, which is \emph{input-specific}, we check
that for a given input specification, the
assumptions actually hold for the generated parameter values.
This gives us an effective verification technique for verifying
generative systems that lies
somewhere between verifying the generator and translation validation.

We carried out the first step of this proof technique for
Muen, using the Spark Ada \cite{gnatpro} verification
environment. The effort involved about 13K lines of source
code and annotation.
No major issues were found, modulo some subjective assumptions we
discuss in Sec.~\ref{sec:discussion}.
We have also implemented a tool that automatically and efficiently
performs the Step~2 check for a given SK configuration.
The tool is effective in proving the assumptions, leading to
machine-checked proofs of correctness for 12 different input
configurations, as well as in detecting issues 
% in generated parameters 
like undeclared sharing of memory components in some seeded faulty
configurations.
%% Of 8 configurations we analyzed, 4 passed the check, leading to
%% machine-checked proofs of correctness for the generated SKs.
%% The other 4 configurations failed the check due to undeclared
%% sharing of writable memory, exposing a potential problem with the
%% generator.

To summarize our contributions, we have proposed a novel approach
based on parametric refinement for verifying generative systems.
We have applied this technique to verify the Muen separation kernel,
which is a complex low-level software system that makes use of hardware
virtualization features.
We believe that other verification efforts for similar generative
systems can benefit from our approach.
To the best of our knowledge our verification of Muen is the first
work that models and 
reasons about Intel's VT-x virtualization features in the context of
separation kernels.
Finally, we note that our verification of Muen is a \emph{post-facto}
effort, in that we verify an existing system which was not designed
and developed hand-in-hand with the verification process.



%% With automation in every domain in the physical world, security-critical systems are not left behind. Satellites, aircrafts, automobile industries, etc. are consistently getting updated with the developments in the computer science. But every now and then we hear about bugs in these systems. What is more important is the kernel on which these systems are running. If the underlying kernel has bugs, the system running on top of it, irrespective of being without bugs, will surely lead to malfunctioning. A separation kernel is one such kernel which has security critical applications, like military applications and avionics.

%% A separation kernel \cite{rushby} is a thin layer of software on top of hardware which simulates a distributed environment on a single machine. By a distributed environment, we mean machines with their own processors and memory, having limited communication channels between them in the form of wires. When simulated on a separation kernel, these multiple machines are called \emph{subjects} and the connections between these machines are called \emph{channels}. Subjects can be as complex as an operating system and as simple as a bare-metal application. \textbf{[A basic block diagram can be added here showing the separation kernel, underlying processor, subjects and communication channels]}

%% In military systems, Multiple Independent Levels of Security (MILS) \cite{harrisonmils2005,foss06mils} environment is implemented by a separation kernel. In an MILS environment there are multiple components which have controlled information flow between them and enforces four properties - Non-bypassable, Evaluatable, Always-invoked, Tamperproof (NEAT). In such systems, one needs to execute multiple systems with different security levels and limited communication. A separation kernel is well suited for that job.

%% In automated aircrafts, multiple features like - auto-pilot, auto-throttle, flight management, etc. are implemented separately. Sensor and control information is the only portion being shared among these systems. Each of these features are implemented as a distinct component and run on a separation kernel.

%% A separation kernel is generally a static system in terms of the number of subjects, memory and other resources. Once the subjects and resources are specified in the policy and the tool-chain generates a system for this policy, it is freezed during execution. New subjects can neither be added nor a subject can be allocated extra memory during execution.	

%% We chose Muen separation kernel \cite{muen} as an exemplar of a modern separation kernel using recents developments in virtualization. Muen is developed by Reto Buerki and Adrian-Ken Rueegsegger at University of Applied Sciences Rapperswil (HSR), Switzerland. It is a separation kernel for Intel x86 platforms. It is written in AdaCore SPARK language, which is a subset of Ada programming language. There are around 2500 lines of SPARK code and 250 lines of assembly. \textbf{Update these numbers according to the version we are verifying.}

%% We proved the functional correctness of the Muen separation kernel using a refinement-based approach. In general refinement technique, given the concrete code and the abstract specification, we need to show that the concrete refines the abstract. But in the case of a separation kernel, like Muen, a new source code is generated every time a new policy is given. And the kernel generator itself is more complex than the kernel which makes it more difficult to reason about the full system (kernel generator and kernel). Hence we come up with a refinement approach for such systems which we call generated systems. In this technique the refinement is shown on a parametrized version of a system for a given policy assuming certain properties on the state of the system. The assumed properties are later on checked everytime a new policy is provided. In this way we prove the correctness for all possible policies. Our technique is described in more detail in section \ref{sec:parametric-refinement}. 

%% The proof was done automatically using AdaCore SPARK tool which is a tool to verify Ada programs. We assumed the following while verifying Muen:
%% \begin{itemize}
%%     \item Loader is functioning correctly as expected.
%% 	\item Hardware is working correctly.
%% 	\item There are no interrupts or events in the system.
%% \end{itemize}
%% \textbf{Add more assumptions}\\
%% \textbf{Note for other members in the project}: Should we move assumptions to proof section?

%% Our contributions in this paper include:
%% \begin{itemize}
%%     \item modelling the behaviour of the Muen separation kernel using a transition system
%%     \item verification of a separation kernel which uses Intel's hardware support for virtualization
%%     \item a light-weight technique to verify programs generated by a complex generator program
%% \end{itemize}
%% %%
%% %%To proceed further, we shall describe the Muen separation kernel briefly here. For more details, one can refer to the Muen report \cite{muen} published by its developers.
%% %%

%% %Modern separation kernels, like Muen, are using hardware support of virtualization. Let us discuss the systems which run on a machine which provides hardware support for virtualization.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main.tex"
%%% End:
