\section{Abstract Specification}
\label{sec:abstract-spec}

We describe an abstract specification $T_C$
that implements in a simple way the intended behaviour of the system
specified by a policy $C$.
In $T_C$ each subject $s$ is run on a \emph{separate}, dedicated,
single-CPU processor system $M_s$.
The system $M_s$ has its own CPU with registers, and $2^{64}$ bytes of
physical memory $\vmem$.
For each subject $s$ we have a similar sized array called
\perms\ which gives the
permissions (read/write/exec/invalid) for each byte in its virtual
address space.

To model shared memory components like channels, we use a separate memory
array $\chmem$, as depicted in % Sec.~\ref{sec:policy}.
Fig.~\ref{fig:channel}.
We assume a
partial function $\cmap_s$ which maps the address space of 
subject $s$ to the channel memory.
% depending on specification in the policy. 
For any access to
memory, it is first checked whether that location is
in a channel using another Boolean function called $\ch_s$,
and if so, the content is updated/fetched from the address 
in \chmem\ given by the $\cmap_s$ function.

There is also a set of \emph{logical} CPUs (the number of CPUs
specified in the policy), and we associate with each logical CPU, the
(disjoint) group of subjects the policy maps to that CPU.

There is no kernel in this system, but a \emph{supervisor} whose job
it is to process events directed to a logical CPU or subject, and
to enable and disable subjects based on the scheduling policy and the
current ``time''.
%% Towards this end it maintains a flag $\enabled_s$ for
%% each subject $s$, which is set whenever the subject is enabled to run
%% based on the current time.
To implement the specified schedule, it uses structures \schedplans\,
and \subjectspecs, similar to the Muen kernel.
However it keeps track of time using the clock-like abstraction
depicted in Fig.~\ref{fig:schedpolicy}(b), with the
per-logical-CPU variables \ticks\ (counts each tick event
modulo the total schedule cycle length), 
\minticks\ (reset at the end of every major frame),
\idealmajfp\ (ideal major frame pointer),
\minorfp\ (minor frame pointer),
and \idealcycles\ (cycle counter).
It also uses a global major frame pointer called \majorfp,
and a global cycle counter \cycles.

%% We use two kinds of pointers here---ideal and global.
%% The ideal pointers on each logical CPU are independent of
%% the others, while global pointers are updated only when every CPU has
%% completed the major frame. This is introduced in order to model
%% synchronization of CPUs. We say a CPU is \emph{enabled} iff both global and
%% ideal values match for both cycles and major frames on that
%% CPU.

%% We also maintain a Boolean array \last\ which keeps track of the
%% CPUs whose tick count are lagging behind the furthest.
%% This is useful while updating the global \cycles\ and
%% \majorfp\ variables.
%% Whenever in every tick event we if the ideal and global values
%% of cycles and \texttt{maj\_fp} do not match we check if it is the only
%% last CPU, then we increment the global values.

In the \init\ operation the supervisor initializes the processor
systems $M_s$, permissions array \perms, the channel memory
\chmem, and also the schedule-related 
variables, based on the policy.
The \execute\ operation, given a logical CPU id, executes the next
instruction on the subject machine currently active for that logical
CPU id.
If the CPU is not enabled, the operation is defined to be a no-op.
An \execute\ operation does not affect the state of other subject processors,
except possibly via the shared memory \chmem.
If the instruction accesses an invalid memory address, the system is
assumed to shut down in an error state.
Finally, for the \event\ operation, which is a tick/interrupt event 
directed to a logical CPU or subject, the supervisor updates the
scheduling state, or pending event table, appropriately.

To represent the system $T_C$ concretely, we use an Ada program which
we call $A_C$.
$A_C$ is a programmatic realization of $T_C$, with processor registers
represented as 64-bit numeric variables, and memory as byte arrays of
size $2^{64}$.
The operations \init, \execute, and \event\ are implemented as methods
that implement the operations as described above.

Finally, we obtain a parametric program $B[\bar{U}]$ from $A_C$, by
parameterizing it as illustrated in
Sec.~\ref{sec:parametric-refinement}.
Thus we declare constants like MAXSub, % MAXCPU, MAXMajfr, and MAXMinfr,
and declare corresponding arrays of these sizes (e.g.\@ the array
of per-subject CPUs will have size MAXSub).
We then declare variables of these size types, like $\ansubs$ (for
``Abstract Number of Subjects'') of type
1..MAXSub,
% $\ancpus$ of type 1..MAXCPUs, an array \vmem\ of size
% TMAXSub, etc,
which will act as parameters to be initialized later.
We call the list of parameters 
% $\ansubs, \ancpus, \aschedplans,
% \asubjectspecs \ldots$ as 
$\bar{U}$.
%
By construction, it is evident that if we use the values $\bar{u}_C$ for
the respective parameters in $\bar{U}$, 
we will get a machine program $B[\bar{u}_C]$
which is equivalent in behaviour to $A_C$.


%% $A_C$ is an array of \emph{machines} indexed by the subject IDs (subjects are numbered from 0 to given number of subjects in the policy) with an extra \emph{channel memory}. A machine is defined as a program which has following global variables - memory, which is an array of bytes, and set of registers, which are 64-bit variables.
%% The operation on such a machine is execute(). execute instruction represents normal CPU instructions like MOV, ADD, etc. which in essence modifies a memory location or a register. There are two kinds of subjects in the Muen system - native and VM subjects. Native subjects are the bare metal applications while VM subjects are like OS. In case of native subjects the access to memory is directly to the memory without paging. In the case of VM subjects, memory is accessed through paging using CR3 register of the machine. We shall see later how the channel memory is being used.

%% In the scheduling policy, as we have seen in section \ref{sec:policy}, scheduling is based on CPUs. Every subject runs for sometime and stops and then again starts when its time comes on its allocated CPU. In $A_C$ we model this by partitioning the subjects in groups of subjects. The subjects in a group run on a single CPU in the concrete.
%% %%% I am not able to explain the role of CPU here

%% Operations on $A_C$ come on a particular CPU. The effect of the operation takes place on the machine corresponding to the currently running subject on the CPU. \texttt{init()}, \texttt{execute(int cpu)} and \texttt{tick(int cpu)} are the methods in this program.

%% Channel memory is a separate memory area for channels. We have a partial function \texttt{cmap()} which maps the address space of the subject to the channel memory depending on the addresses which are part of a channel according to the policy. For any update to the memory of the subject, it is first checked whether that location is channel or not by using another Boolean function called \texttt{ch()}, then if it is part of a channel content is updated at the place where the \texttt{cmap()} function maps the given address to. We also have the notion of number of CPUs not for processing but just to follow the scheduling policy which is mentioned per CPU. So here subjects which are allocated to the same CPU will never run in parallel while the ones running on different CPUs can run in parallel.

%% For managing scheduling, our abstract state contains the following:
%% \begin{itemize}
%% 	\item \texttt{ticks} : \texttt{ticks} are reset at the end of every cycle of the clock
%% 	\item \texttt{min\_ticks} : \texttt{min\_ticks} are reset at the end of every major frame
%% 	\item \texttt{major\_fp} : global major frame pointer
%% 	\item \texttt{ideal\_maj\_fp} : major frame pointer for each CPU
%% 	\item \texttt{minor\_fp} : minor frame pointer for each CPU
%% 	\item \texttt{cycles} : global cycle counter
%% 	\item \texttt{ideal\_cycles} : cycle counter for each CPU
%% \end{itemize}

%% To explain the use of these components of the abstract state let us take the example in figure \ref{fig:schedpolicy_clock} and see how these are updated. Initially the values of all the above components are 0. The values of \texttt{ticks} and \texttt{min\_ticks} keep on increasing at every tick event by 1. When the value of \texttt{min\_ticks} is equal to the deadline of the current minor frame (40 for CPU0 in the example), the \texttt{min\_fp} is incremented. When it is equal to the length of the major frame, which is 80 for the first major frame in the example, the value of \texttt{min\_ticks} is reset and the \texttt{ideal\_maj\_fp} is incremented. In essence, \texttt{min\_ticks} keep track of time within a major frame. When the value of \texttt{ticks} is equal to the length of the full \emph{cycle} of the scheduling policy, 200 (80 for major frame 0 + 120 for major frame 1) in the example, \texttt{ticks} value is reset. \texttt{ideal\_cycles} is incremented when the cycle is completed.

%% We are using here two kinds of values of major frame pointer and cycles - ideal and global. Ideal values on each CPU are independent of other CPUs while global values are updated only when every CPU has completed the major frame. This is introduced in order to model synchronisation of CPUs. We say a CPU is enabled if both global and ideal values match for both cycles and \texttt{maj\_fp} on that CPU. If global and ideal values do not match for either of cycles or \texttt{maj\_fp} or both then the CPU is in a disabled state. Any operation on that CPU will not have any effect (like No Op).

%% As discussed earlier we are only modelling two events (in addition to \texttt{init}) in our work - local memory/register update and tick.
%% In local memory update for example write to an address. We directly write to the address in the subject's address space as in the abstract every subject has its own memory rather than sharing a big memory (physical memory in the concrete). In case the subject is allowed to use paging when it is an OS, it will do normal transalation using its own CR3, and then write to the subject's memory. There is no another level of translation as in the concrete system.

%% In case of tick operation the updates are already roughly explained with the help of the example above. \texttt{min\_ticks} and \texttt{ticks} are incremented at every tick. If the \texttt{min\_ticks} value becomes equal to the deadline of the current minor frame, pointed to by \texttt{min\_fp}, \texttt{min\_fp} is incremented, if it is not pointing to last minor frame. If \texttt{min\_fp} points to the last minor frame in the current major frame, pointed to by \texttt{ideal\_maj\_fp}, \texttt{ideal\_maj\_fp} is incremented and \texttt{min\_fp} and \texttt{min\_ticks} are reset, if it is not pointing to the last major frame in the cycle. If the \texttt{ideal\_maj\_fp} was pointing to the last major, \texttt{ideal\_cycles} is incremented and \texttt{ideal\_maj\_fp} and \texttt{ticks} are reset. This was the update of the ideals.

%% We also maintain a Boolean array \texttt{last} to keep track of the CPUs which are behind or at least with the others but not ahead of the others. Whenever in every tick event we if the ideal and global values of cycles and \texttt{maj\_fp} do not match we check if it is the only last CPU, then we increment the global values.


%The state has the following components:
%\begin{itemize}
%	\item n is the number of subjects

%	\item $M_i:\mathbb{N}\rightarrow \mathbb{B}^w$ is the memory state of the subject $i$. This is a partial function where only the addresses which are in the address space of the subject are mapped to a word. $w$ is the word size. There is a bit assigned with each address specifying the write permission. The channel is also part of this memory where the write permission depends upon the access rights of the subject to the channel as defined in the policy.
	
%	\item $R_i:R\rightarrow \mathbb{B}^w$ is the register state of the subject $i$. Here $R$ is the set of registers.
	
%	\item $ticks$ maintains the tick count of the system. $ticks \in \mathbb{N}^m$ where each natural number component represents the tick for corresponding $P_i$ or CPU. More on ticks in later sections.

%	\item $min\_ticks$

%	\item $maj\_fp \in\{x:x\in \mathbb{N},1\le x\le p\}$ tells us which major frame is going on currently. p is the constant which represents the number of major frames, is defined in the policy.

%	\item $min\_fp \in\mathbb{N}^m$ tells us which minor frame is currently running in each of the $P_i$ or CPU. $m$ is the constant as mentioned in the policy which represents the number of CPUs or elements in $P$.

%	\item $sched\_plans$

%	\item $mfe$
%\end{itemize}

%There are 4 kinds of transitions in our abstract specification:
%\begin{enumerate}
%	\item Local operations - These are the operations which do not affect the memory or registers of the other subjects.
%	\item Tick - This operation models the time
%	\item Interrupt
%	\item Event
%\end{enumerate}
%Now we shall discuss each of these operations and their effects in the following subsections.

%\subsection{Local operations}
%As an example of a local operation, let us take write operation, which given a cpu, address and data, writes at that address the given data for the subject currently running on the given cpu.

%\subsection{Tick operation}	
