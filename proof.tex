\section{Parametric Refinement Proof}
\label{sec:proof}

We now show that the parametric version of the Muen system
$Q[\bar{V}]$ conditionally refines the parametric abstract spec
$B[\bar{U}]$.
From Sec.~\ref{sec:parametric-refinement}, this requires us
to identify the condition $R$, and find a gluing relation
$\rho$ on the state of parametric programs $Q$ and $B$ such that the
refinement conditions (init) and (sim) are satisfied.

We use a condition $R$ whose key conjuncts are the following conditions:
\begin{itemize}
\item $R_1$: The page tables $\pt_s$ associated with a subject $s$
  must be \emph{injective} in that no two virtual addresses, within a
  subject or across subjects, may be mapped to the same physical
  address, \emph{unless} they are specified to be part of a shared
  memory component.
  More precisely, for each $s,s'$, and addresses $a \in \vmem_s$, $a'
  \in \vmem_{s'}$, we have:
  \begin{enumerate}
  \item ($\ch_s(a) \,\myAnd\, \ch_{s'}(a') \,\myAnd\, \cmap_s(a) =
  \cmap_{s'}(a'))$ \\
    $\implies \pt_s(a) = \pt_{s'}(a')$;
  \item $[s \neq s' \myAnd \neg (\ch_s(a) \,\myAnd\, \ch_{s'}(a')
    \,\myAnd\, \cmap_s(a) = \cmap_{s'}(a'))]$ 
    $\implies \pt_s(a) \neq \pt_{s'}(a')$; and
  \item $[a \neq a' \myAnd \neg (\ch_s(a) \,\myAnd\, \ch_{s'}(a')
    \,\myAnd\, \cmap_s(a) = \cmap_{s'}(a'))]$
    $\implies \pt_s(a) \neq \pt_{s'}(a')$.
  \end{enumerate}
  %% For each subject $s$, $\pt_s$ must be \emph{injective} in
  %% that no two
  %% addresses are mapped by the page table to the same physical
  %% address. Further, addresses across subjects must be injective (for
  %% no two subjects $s$ and $s'$, and addresses $a$ and $a'$, should
  %% $\pt_s(a) = \pt_{s'}(a')$, \emph{unless} $a$ and $a'$ are part of the
  %% same channel or a read-only memory component.
\item $R_2$: For each subject $s$, the permissions (rd/wr/ex/present)
  associated with an address $a$ should match with the permissions for
  $a$ in $\pt_s$.
\item $R_3$: For each subject $s$, no invalid virtual address is mapped to a physical address by page table $\pt_s$.
\item $R_4$: For each \emph{valid} address $a$ in a subject $s$
  % (i.e.\@ $a$ belongs to one of the memory components of $s$), 
  the
  initial contents of $a$ in $\vmem_s$ should match with that of 
  $\pt_s(a)$ in the physical memory $\pmem$.
\item $R_5$: The values of the parameters (like \nsubs,
  \subjectspecs, \schedplans\ and \IOBitmap) in the concrete should
  match with those in the abstract.
%% \item $R_6$: The deadlines of all the minor frames in \schedplans{} are > 0 and < $2^{32}$ and are in strictly increasing order in a major frame.
%% \item $R_7$: The deadline of the last minor frame in a major frame in \schedplans{} is equal to the \texttt{Major\_Frames} value indexed at that major frame.
\end{itemize}


%% In this section we are going to talk about how we proved the parametric conditional refinement between $Q[\bar{V}]$ and $B[\bar{U}]$.

%% As briefly described in section \ref{sec:parametric-refinement}, we do this by taking a combined abstract-concrete program. Here we define a combined state having both abstract and concrete variables. We also define the combined methods \texttt{combined\_init()}, \texttt{combined\_write()} and \texttt{combined\_tick()} for \texttt{init()}, \texttt{write()} and \texttt{tick} respectively. We juxtaposed the methods defined in the previous sections to define the combined methods. Again as mentioned in section \ref{sec:parametric-refinement}, to prove the refinement we need to define the gluing relation and prove (init) and (sim) conditions.

The gluing relation $\rho$ has the following key conjuncts:
\begin{enumerate}
\item The CPU register contents of each subject in the abstract
  match with the register contents of the CPU on which the subject
  is active, if the subject is enabled, and with the subject
  descriptor, otherwise.
  % or with the values of the registers in the subject
  % descriptor (if it is not enabled).
\item For each subject $s$, and valid address $a$ in its virtual
  address space, the contents of $\vmem_s(a)$ and $\pmem(\pt_s(a))$
  should match.
\item The value $(\mathrm{TSC} - \mathrm{CMSC})$ on each CPU in the
  concrete, should 
  match with how much the ideal clock for the subject's logical CPU is
  ahead of the beginning of the current major frame in the abstract.
%% TSC - CMSC on every CPU should be equal to the difference between the current position marked by (\texttt{ideal\_cycles}, \texttt{ideal\_maj\_fp}, \texttt{min\_fp}, \texttt{ticks} and \texttt{min\_ticks}) and the position where the current major frame started, \texttt{Maj\_Frames\_End[maj\_fp - 1]}
\item The major frame pointer in the abstract and concrete should
  coincide, and the minor frame pointers should agree for enabled CPUs.
%% The major frame pointer in the concrete, called \texttt{current\_major\_frame} is equal to the global major frame pointer in the abstract, called \texttt{maj\_fp}
%% \item The minor frame pointers in the abstract and the concrete agree
%%   on each CPU whenever the corresponding CPUs are enabled in the
%%   abstract.
%% minor frame pointers in the abstract (\texttt{min\_fp}) and the concrete (\texttt{current\_minor\_frame}) are equal on each CPU whenever the corresponding CPUs are enabled in the abstract.
\item On every enabled CPU, the sum of the VMX Timer and \minticks\
  should equal the deadline of the current minor frame on that CPU.
\item A CPU is waiting in a barrier in the concrete whenever the CPU
  is disabled in the abstract and vice-versa.
\item The contents of the abstract and concrete pending event tables 
% \apeventtab\ and \peventtab\ 
should agree.
\end{enumerate}

%% To check (init) condition, we assume the condition $R$ of the
%% conditional parametric refinement, described at the end of this
%% section, before the start of the \texttt{combined\_init} procedure
%% i.e., as the precondition of the procedure, and prove that the gluing
%% relation holds at the end of the procedure, i.e. as the post
%% condition. So basically it is proving the correctness of the Hoare
%% triple - \{$R$\} \texttt{combined\_init} \{$\rho$\}

%% To check (sim) condition we assume that the gluing relation and the condition $R$ as the precondition of the operation and proving the gluing relation again as the post condition of the same operation. This is again equivalent to proving the correctness of the Hoare triple - \{$R \land \rho$\} \texttt{combined\_op} \{$\rho$\}	

%% To automate the whole proof, we used SPARK tool and its language which is also called SPARK language.
%% SPARK language provides datatypes similar to Ada programming language. We used the \texttt{record} type to define our states. SPARK allows asserting and assuming conditions on the variables of the programs using \texttt{pragma assert()} and \texttt{pragma assume()} annotations respectively. Similarly precondition and postcondition can be mentioned for every procedure. We used these annotations to implement our proof strategy.

%% %In the combined functions, the abstract part is defined according to the abstract specifications discussed in the previous section. The concrete part is taken from the Muen source code with slight modification to make it a generalized system as per our technique.

%% SPARK tool converts the given program with the annotations into verification conditions in terms of logical formulae and then passes it to the theorem prover. In the backend SPARK uses three theorem provers - Z3, CVC4 and alt-ergo. We ran the proof for the three methods - \texttt{combined\_init}, \texttt{combined\_write} and \texttt{combined\_tick}. 

%% There were cases where the prover was not able to prove some of the conditions. We had to give hints to the prover in the form of assertions in the program. In some cases we also needed to add auxiliary variables in the concrete state.

We carry out the refinement check in a similar way to standard
non-parametric machine programs.
We construct a ``combined'' version of
$Q$ and $B$ that has the disjoint union of their state variables, as
well as a joint version of their operations, and phrase the
refinement conditions as pre/post conditions on the joint operations.
In particular, for the (init) condition we need to show that assuming
the condition $R$ holds on the parameters, if we carry out the joint
init operation, then the resulting state satisfies the gluing relation
$\rho$.
To check the (sim) condition for an operation $n$, we assume a joint
state in which the gluing relation $\rho$ holds, and then show that the state
resulting after performing the joint implementation of $n$, satisfies
$\rho$. Once again this is done assuming that the parameters satisfy
the condition $R$.
%
We carry out these checks using the Spark Ada tool \cite{gnatpro}
which given an Ada
program annotated with pre/post assertions, generates verification
conditions and checks them using provers Z3 \cite{Z3}, CVC4
\cite{BarrettCDHJKRT11}, and Alt-Ergo \cite{conchon12}.

We faced several challenges in carrying out this proof to completion.
% We highlight the key challenges we faced and how we overcame them.
A basic requirement of the gluing relation $\rho$ is that the abstract
%% \begin{wrapfigure}[4]{r}{0.29\textwidth}
%% % \begin{table}
%% % \centering
%% % \vspace{-0.6cm}
%% % \begin{scriptsize}
%% \begin{small}
%% 	\begin{tabular}{|c|c|c|c|}
%% 		\hline
%% 		Artifact & $B[\bar{U}]$ & $Q[\bar{V}]$ & Combined\\
%% 		\hline
%% 		LOC+LOA & 793 & 1914 & 13491\\
%% 		\hline
%% 	\end{tabular}
%% %\end{scriptsize}
%% \end{small}
%% % \caption{Proof artifact sizes}
%% % \label{table:proof-result}
%% % \end{table}
%% \end{wrapfigure}
and physical memory 
contents coincide via the page table map for each
subject.
After a write instruction, we need to argue that this property
continues to hold.
However, even if one were to reason about a given concrete page
table, the prover would never be able to handle this due to the sheer
size of the page table.
The key observation we needed was that the actual mapping of the page table was
irrelevant: all one needs is that the mapping be \emph{injective} as
in condition $R_1$.
With this assumption the proof goes through easily.
%
A second challenge was proving the correctness of the way the kernel
handles the tick event.
This is a complex task that the kernel carries out, requiring 8
subcases to break up the reasoning into manageable subgoals for both
the engineer and the prover.
The presence of the barrier synchronization before the beginning of a
new major frame, adds to the complexity.
The use of auxiliary variables (like an array of \last\ CPUs), and 
the case-split helped us to carry out this proof
successfully.
Finally, modelling the IO system, including injection of interrupts in
a virtualized setting, was a complex task.
Table~\ref{table:proof-result}
shows details of our proof effort
in terms of lines of code (LoC) and lines of annotations(LoA) in the
combined proof artifact.

\begin{table}
\centering
% \vspace{-0.6cm}
% \begin{small}
	\begin{tabular}{|c|c|c|c|}
		\hline
		Artifact & $B[\bar{U}]$ & $Q[\bar{V}]$ & Combined\\
		\hline
		LoC+LoA & 793 & 1,914 & 15,754\\
		\hline
	\end{tabular}
% \end{small}
\vspace{0.2cm}
\caption{Proof artifact sizes}
\label{table:proof-result}
\end{table}



%% The property $R1$ is important because if we do not have it, we shall need the concrete translation function to prove that after the update to the memory using write instruction, the page tables and the memory are still glued. In the parametric program it would be difficult for the theorem prover to prove without knowing the content of the page tables. So we saw that this is the property which is requierd for the separation of memories and it reduces the difficulty to a great extent.

%% Another difficulty we faced was in proving the correctness of the scheduling in tick operation. Theoretically a CPU may wait for a long period of time in the barrier, such that it can wait ticks equal to the length a major frame. We have assumed that this case never occurs.
% couldn't think of anything else in scheduling to write... I think
% rest is covered in previous sections.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main.tex"
%%% End:
