\section{Policy Input Specification}
\label{sec:policy}

The input specification to Muen is an XML file called a \emph{policy}.
It specifies details of the host processor, subjects to be run, and a precise
schedule of execution on each CPU of the host processor system.
% It is presented as an XML file whose key components are described below.
%
% \paragraph{Host Hardware}
Details of the host processor include the
number of % logical 
CPUs, the frequency of the processor, and
the available host physical memory regions.
It also specifies for each device (like a keyboard) the
IO Port numbers, the IRQ number, the vector number, and finally
the subject to which the interrupt should be directed.

% \paragraph{Subject details}
The policy specifies a set of named \emph{subjects},
and, for each subject, the size and starting addresses of
the components in its virtual memory.
These components include the raw binary image of the subject, 
%% \begin{wrapfigure}[9]{r}{0.25\textwidth}
%%   \centering
%% \vspace{-0.3cm}
%% \input{channel-v2.pdf_t}
%% % \caption{A memory channel}
%% % \label{fig:channel}
%% \end{wrapfigure}
as well as possible shared
memory ``channels'' it may have declared.
A \emph{channel} is a memory component that can be
shared between two or more subjects.
The policy specifies the size of each channel,
and each subject that uses the channel specifies
the location of the channel in its virtual address space,
along with read/write permissions to the channel.
Fig.~\ref{fig:channel} % figure alongside 
depicts a channel $\mathit{Chan}$
shared by subjects
$\mathit{Sub0}$ (with write permission) and $\mathit{Sub1}$ (with
read permission).

\begin{figure}
	\centering
	\input{channel-v2.pdf_t}
	\caption{A memory channel shared by two subjects}
	\label{fig:channel}
\end{figure}

% \paragraph{Schedule}
The schedule is a sequence of \emph{major frames},
that are repeated in a cyclical fashion.
Each major frame specifies a schedule of execution for each CPU,
for a common length of time.
Time is measured in terms of \emph{ticks}, a unit of time
specified in the policy.
A major frame specifies for each CPU a sequence of \emph{minor
  frames}.
Each minor frame specifies a length in ticks, and the subject
to run in this frame.
% For a given major frame, the cumulative length of
% minor frames on each CPU, must be the same.
A subject can be assigned to only one CPU in the
schedule.
%
An example scheduling policy in XML is shown in
Fig.~\ref{fig:schedpolicy}(a), while
Fig.~\ref{fig:schedpolicy}(b) shows the same schedule viewed as
a clock. Each CPU is associated with one track in the clock.
The shaded portion depicts the passage of time (the tick count) on
each CPU.
%% Subjects 1 and 2 are
%% allocated to CPU 0. Subjects 3 and 4 are allocated to CPU 1. On
%% CPU 0 subject 1 will run for the first 40 ticks, then for the next
%% 40 ticks subject 2 will run and reaches the end of major frame
%% 0. On CPU 1, subject 3 runs for 80 ticks and reaches the end of
%% major frame 0 where both CPUs will synchronise. After the two CPUs
%% synchronise, the major frame 1 starts. The number of ticks in a
%% major frame remains the same on all the CPUs. As here in the major
%% frame 0, total number of ticks on each CPU is 80 ticks. Similarly
%% it is 120 ticks on both CPUs in the major frame 1. In major frame
%% 1, subject 1 gets to execute on CPU 0 for 80 ticks and subject 4
%% on CPU 1 for 60 ticks. After subject 1 finishes 80 ticks, subject
%% 2 executes for 40 ticks. On CPU 1, after 60 ticks, subject 3 runs
%% for 60 ticks. Now again both the CPUs synchronise. Since, Muen
%% uses round-robin scheduling, the major frame 0 starts again. This
%% is demonstrated in figure \ref{fig:schedpolicy_clock} as a clock where
%% pointers to the minor and the major frame are like hands of a
%% clock and it keeps on moving around the dial.

\begin{figure}
% \vspace{-0.5cm}
\begin{minipage}{5cm}
\centering
\begin{scriptsize}
%\begin{tiny}
\begin{verbatim}
<scheduling tick_rate="10000">
 <major_frame>
  <cpu id="0">
   <minor_fr sub_id="1" ticks="40"/>
   <minor_fr sub_id="2" ticks="40"/>
  </cpu>
  <cpu id="1">
   <minor_fr sub_id="3" ticks="80"/>
  </cpu>
 </major_frame>
 <major_frame>
  <cpu id="0">
   <minor_fr sub_id="1" ticks="80"/>
   <minor_fr sub_id="2" ticks="40"/>
  </cpu>
  <cpu id="1">
   <minor_fr sub_id="4" ticks="60"/>
   <minor_fr sub_id="3" ticks="60"/>
  </cpu>
 </major_frame>
</scheduling>
\end{verbatim}
% \end{tiny}
\end{scriptsize}
\end{minipage} \\[0.3cm]
\begin{minipage}[b]{5cm}
\begin{center}
\begin{small}
  (a)
\end{small}
\end{center}
\end{minipage}\\[0.75cm]
%% \begin{minipage}{0.9\textwidth}
%% \quad \quad \quad \quad
%% \end{minipage}\\[0.5cm]
\begin{minipage}{8cm}
\input{xml-cycle-rep.pdf_t}
\end{minipage} \\[0.5cm]
%
\begin{minipage}[b]{5.6cm}
\begin{center}
\begin{small}
(b)
\end{small}
\end{center}
\end{minipage}
%
\begin{minipage}[b]{2cm}
\begin{center}
\begin{small}
\ \ \ \ \ \ \ \ \ \ \ (c)
\end{small}
\end{center}
\end{minipage}
\caption{(a) An Example schedule,
(b) it's clock view, and (c) it's implementation in the Muen kernel.}
\label{fig:schedpolicy}
\end{figure}

%% \begin{figure}
%% 	\centering
%% 	\input{xml-cycle-rep.pdf_t}
%% 	\caption{(a) Clock view of a schedule and (b) its
%%           implementation in the Muen kernel.}
%% 	\label{fig:schedpolicy_clock}
%% \end{figure}

Ticks are assumed to occur independently on each CPU, and
can result in a drift between the times on different CPUs.
The scheduling policy requires that before beginning a new frame,
\emph{all} CPUs must complete the previous major frame.
The end of a major frame is thus a synchronization point
for the CPUs.
%% An important consequence of this is that some subjects may miss a
%% part (or all of) their time slice (i.e. a minor frame).
%% Consider the scenario in the example schedule where the tick count
%% on CPU1 is ahead of 
%% CPU0 by 20 ticks, at the point of time when CPU0 finally finishes
%% the first major frame. Thus the tick count on CPU0 is 80 while on
%% CPU1 it is 100.
%% Now the second major frame can begin, but Sub~4 which was
%% scheduled in the next minor frame on CPU1, will get to execute for
%% only 40 of its 60 ticks. Thus it loses 20 ticks due to the fact
%% that CPU0 finished its previous major frame late.

%% We assume that a policy satisfies certain \emph{validity} conditions.
%% These include, for example, the requirement that the sum of ticks of all
%% the minor frames in a major frame must be the same across all the
%% CPUs, and that each subject is mapped to exactly one CPU.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main.tex"
%%% End:
